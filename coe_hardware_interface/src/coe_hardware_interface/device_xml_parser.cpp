#include <coe_hardware_interface/device_xml_parser.h>

namespace hardware_interface 
{

  
DeviceParser::DeviceParser(const std::__cxx11::string& deviceclass_name)
{
  m_deviceclass_name = deviceclass_name;
}
  
bool DeviceParser::parseXml(const std::__cxx11::string& xml_value)
{

  pt::ptree tree;
  
  std::stringstream ss; 
  ss << xml_value;
  pt::read_xml(ss, tree);

  pt::ptree robot;
  pt::ptree device;
  bool found=false;
  
  for(const pt::ptree::value_type &v : tree)
  {
    if (!v.first.compare("robot"))
    {
      ROS_DEBUG("Find robot called %s",v.second.get<std::string>("<xmlattr>.name").c_str());
      robot=v.second;
      break;
    }
  }
  
  for(const pt::ptree::value_type &c : robot)
  {
    if (!c.first.compare("deviceclass"))
    {
      if (!c.second.get<std::string>("<xmlattr>.name").compare(m_deviceclass_name))
      {
        found=true;
        ROS_DEBUG("Find Deviceclass called %s",c.second.get<std::string>("<xmlattr>.name").c_str());
        device=c.second;
        break;
      }
    }
  }
  
  
 
  if (!found)
  {
    ROS_ERROR("Deviceclass %s not found",m_deviceclass_name.c_str());
    return false;
  }
  
  for(const pt::ptree::value_type &r : device)
  {
    if (!r.first.compare("register"))
    {
      m_type.push_back(r.second.get<std::string>("<xmlattr>.type"));
      m_name.push_back(r.second.get<std::string>("<xmlattr>.name"));
      m_scale.push_back(r.second.get<double>("<xmlattr>.scale"));
      m_offset.push_back(r.second.get<double>("<xmlattr>.offset"));
    }
  }
  
  computeMemSize();
  
  
}

bool DeviceParser::parseYaml(const XmlRpc::XmlRpcValue& config)
{
  if (config.getType() != XmlRpc::XmlRpcValue::TypeArray)
  {
    ROS_ERROR("The param is not a list of fields" );
    return false;
  }
  
  ROS_DEBUG_STREAM("Scan of feasible fields");
  for(size_t i=0; i < config.size(); i++) 
  {
    XmlRpc::XmlRpcValue field = config[i];  
    if( field.getType() != XmlRpc::XmlRpcValue::TypeStruct)
    {
      ROS_WARN("The element #%zu is not a struct", i);
      continue;
    }
    if( !field.hasMember("name") )
    {
      ROS_WARN("The element #%zu has not the field 'name'", i);
      continue;
    }
    if( !field.hasMember( "scale" ) )
    {
      ROS_WARN("The element #%zu has not the field 'scale'", i);
      continue;
    }
    if( !field.hasMember( "offset" ) )
    {
      ROS_WARN("The element #%zu has not the field 'offset'", i);
      continue;
    }

    
    
    m_type.push_back(field["type"]);
    m_name.push_back(field["name"]);
    m_scale.push_back(field["scale"]);
    m_offset.push_back(field["offset"]);
  }
  computeMemSize();
  
}

void DeviceParser::computeMemSize()
{
  for (unsigned int idx=0;idx<m_type.size();idx++)
    ROS_DEBUG("%s (%s) scale=%f, offset=%f",m_name.at(idx).c_str(),m_type.at(idx).c_str(),m_scale.at(idx),m_offset.at(idx));
  
  m_int8_t_size=0;
  for (std::string s: m_type)
  {
    if (!s.compare("double"))
    {
      m_int8_t_size+=sizeof(double);
    }
    else if (!s.compare("float"))
    {
      m_int8_t_size+=sizeof(float);
    }
    else if (!s.compare("int16_t"))
    {
      m_int8_t_size+=sizeof(int16_t);
    }
    else if (!s.compare("uint16_t"))
    {
      m_int8_t_size+=sizeof(uint16_t);
    }
    else if (!s.compare("int8_t"))
    {
      m_int8_t_size+=sizeof(int8_t);
    }
    else if (!s.compare("uint8_t"))
    {
      m_int8_t_size+=sizeof(uint8_t);
    }
    else
    {
      ROS_ERROR("Type %s is not implemented yet!",s.c_str());
    }
  }
}


DeviceParser::~DeviceParser()
{
  m_mapmem.reset();
  m_mtx.reset();
  boost::interprocess::shared_memory_object::remove(m_mem_name.c_str());
  
}


void DeviceParser::mapMemory(const std::__cxx11::string& mem_name, const bool& read_only, const bool& open_only)
{
  m_mem_name=mem_name;
  
  if (m_int8_t_size==0)
    return;
  
  try
  {  
    m_mtx.reset(new boost::interprocess::named_mutex(boost::interprocess::open_or_create, (mem_name+"_mtx").c_str()));
  }
  catch(std::exception& e)
  {
    ROS_ERROR("what: %s",e.what());
    return;
  }
  
  
  try
  {  
    if (open_only)
    {
      if (read_only)
      {
        boost::interprocess::shared_memory_object memobj(boost::interprocess::open_only,mem_name.c_str(),boost::interprocess::read_only);
        m_memobj.swap(memobj);
      }
      else
      {
        boost::interprocess::shared_memory_object memobj(boost::interprocess::open_only,mem_name.c_str(),boost::interprocess::read_write);
        m_memobj.swap(memobj);
      }
      long int size=0;
      m_memobj.get_size(size);
      if (size!=m_int8_t_size)
      {
        ROS_ERROR("Memory size is wrong");
        return;
      }
    }
    else
    {
      if (read_only)
      {
        boost::interprocess::shared_memory_object memobj(boost::interprocess::open_or_create,mem_name.c_str(),boost::interprocess::read_only);
        
        
        m_memobj.swap(memobj);
      }
      else
      {
        boost::interprocess::shared_memory_object memobj(boost::interprocess::open_or_create,mem_name.c_str(),boost::interprocess::read_write);
        long int size=0;
        memobj.get_size(size);
        if (size<m_int8_t_size)
          memobj.truncate(m_int8_t_size);
        m_memobj.swap(memobj);
      }
    }
  }
  catch(std::exception& e)
  {
    ROS_ERROR("what: %s",e.what());
    return;
  }
  
  try
  {  
    if (read_only)
      m_mapmem.reset(new boost::interprocess::mapped_region(m_memobj, boost::interprocess::read_only));
    else
      m_mapmem.reset(new boost::interprocess::mapped_region(m_memobj, boost::interprocess::read_write));
  }
  catch(std::exception& e)
  {
    ROS_ERROR("what: %s",e.what());
    return;
  }
  
}


bool DeviceParser::read(std::map< std::__cxx11::string, double >& data)
{
  if (m_int8_t_size==0)
    return true;
  
  if (!m_mapmem)
  {
    ROS_ERROR("No memories mapped");
    return false;
  }
  if (data.size()!=m_type.size())
  {
    ROS_ERROR("Dimensions are wrong");
    return false;
  }
  
  std::vector<int8_t> copy_of_memory((int)(m_mapmem->get_size()));
  int8_t* ptr=&(copy_of_memory.at(0));
  
  
  m_mtx->lock();
  std::memcpy(ptr,m_mapmem->get_address(),m_mapmem->get_size());
  m_mtx->unlock();
  
  for (unsigned idx = 0; idx<m_type.size();idx++)
  {
    double el;
    if (!m_type.at(idx).compare("double"))
    {
      std::memcpy(&el,ptr,sizeof(double));
      ptr+=sizeof(double);
    }
    else if (!m_type.at(idx).compare("float"))
    {
      float tmp;
      std::memcpy(&tmp,ptr,sizeof(float));
      ptr+=sizeof(float);
      el=double(tmp);
    }
    else if (!m_type.at(idx).compare("int16_t"))
    {
      int16_t tmp;
      std::memcpy(&tmp,ptr,sizeof(int16_t));
      ptr+=sizeof(int16_t);
      el=double(tmp);
    }
    else if (!m_type.at(idx).compare("uint16_t"))
    {
      uint16_t tmp;
      std::memcpy(&tmp,ptr,sizeof(uint16_t));
      ptr+=sizeof(uint16_t);
      el=double(tmp);
    }
    else if (!m_type.at(idx).compare("int8_t"))
    {
      int8_t tmp;
      std::memcpy(&tmp,ptr,sizeof(int8_t));
      ptr+=sizeof(int8_t);
      el=double(tmp);
    }
    else if (!m_type.at(idx).compare("uint8_t"))
    {
      uint8_t tmp;
      std::memcpy(&tmp,ptr,sizeof(uint8_t));
      ptr+=sizeof(uint8_t);
      el=double(tmp);
    }
    else
    {
      ROS_ERROR("Type %s is not implemented yet!",m_type.at(idx).c_str());
      return false;
    }
    
    if (data.find(m_name.at(idx)) == data.end())
    {
      ROS_ERROR("Field %s not present",m_name.at(idx).c_str());
      return false;
    }
    data.at(m_name.at(idx))=m_scale.at(idx)*el-m_offset.at(idx);
  }
  return true;
}

bool DeviceParser::write(const std::map< std::__cxx11::string, double >& data)
{  
  if (m_int8_t_size==0)
    return true;

  if (!m_mapmem)
  {
    ROS_ERROR("No memories mapped");
    return false;
  }
  if (data.size()!=m_type.size())
  {
    ROS_ERROR("Dimensions are wrong");
    return false;
  }
 
  if (m_mapmem->get_mode()==boost::interprocess::read_only)
  {
    ROS_ERROR("Mapped region is a read_only memory");
    return false;
  }
  
  
  std::vector<int8_t> copy_of_memory((int)(m_mapmem->get_size()));
  int8_t* ptr=&(copy_of_memory.at(0));
  
  
  
  for (unsigned idx = 0; idx<m_type.size();idx++)
  {
    if (data.find(m_name.at(idx)) == data.end())
    {
      ROS_ERROR("Field %s not present",m_name.at(idx).c_str());
      return false;
    }
    
    double el=(data.at(m_name.at(idx))+m_offset.at(idx))/m_scale.at(idx);
    if (!m_type.at(idx).compare("double"))
    {
      std::memcpy(ptr,&el,sizeof(double));
      ptr+=sizeof(double);
    }
    else if (!m_type.at(idx).compare("float"))
    {
      float tmp=(float)el;
      std::memcpy(ptr,&tmp,sizeof(float));
      ptr+=sizeof(float);
    }
    else if (!m_type.at(idx).compare("int16_t"))
    {
      int16_t tmp=(int16_t)el;;
      std::memcpy(ptr,&tmp,sizeof(int16_t));
      ptr+=sizeof(int16_t);
    }
    else if (!m_type.at(idx).compare("uint16_t"))
    {
      uint16_t tmp=(uint16_t)el;
      std::memcpy(ptr,&tmp,sizeof(uint16_t));
      ptr+=sizeof(uint16_t);
    }
    else if (!m_type.at(idx).compare("int8_t"))
    {
      int8_t tmp=(int8_t)el;
      std::memcpy(ptr,&tmp,sizeof(int8_t));
      ptr+=sizeof(int8_t);
    }
    else if (!m_type.at(idx).compare("uint8_t"))
    {
      uint8_t tmp=(uint8_t)el;
      std::memcpy(ptr,&tmp,sizeof(uint8_t));
      ptr+=sizeof(uint8_t);
    }
    else
    {
      ROS_ERROR("Type %s is not implemented yet!",m_type.at(idx).c_str());
      return false;
    }
    
  }
  
  m_mtx->lock();
  std::memcpy(m_mapmem->get_address(),&(copy_of_memory.at(0)),m_mapmem->get_size());
  m_mtx->unlock();
  
  return true;
}

std::map< std::__cxx11::string, double > DeviceParser::getEmptyMap()
{
  std::map< std::string, double >  map;
  for (std::string& s: m_name)
  {
    map.insert(std::pair< std::__cxx11::string, double >(s,0.0));
  }
  return map;
}


}