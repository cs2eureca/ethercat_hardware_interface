#include "ros/ros.h"
#include <coe_hardware_interface/device_xml_parser.h>

int main(int argc, char **argv){
  ros::init(argc, argv, "xml_test");
  ros::NodeHandle nh;

  std::string xml;
  if (!  nh.getParam("robot_description",xml))
  {
    ROS_ERROR("el3008 does not exist");
  }

  hardware_interface::DeviceParser parser_write("el3008");
  parser_write.parseXml(xml);
  
  
  XmlRpc::XmlRpcValue yaml;
  if (!nh.getParam("/el3008",yaml))
    return false;
  hardware_interface::DeviceParser parser_read("el3008");
  parser_read.parseYaml(yaml);
  
  
  std::map<std::string,double> map=parser_read.getEmptyMap();
  for (auto& p: map)
    p.second=1;
    
  ROS_INFO("WRITE:\n");
  for (auto& p: map)
    ROS_INFO("%s : %f",p.first.c_str(),p.second);

  std::string memory_name="pippo"; //"shared_memory"
  parser_write.mapMemory(memory_name,false,false);
  parser_write.write(map);
      
  parser_read.mapMemory(memory_name,true,true);
  ROS_INFO("READ:\n");
  if (parser_read.read(map))
    for (auto& p: map)
      ROS_INFO("%s : %f",p.first.c_str(),p.second);
  else
    ROS_ERROR("Unable to read memory");

  
  return 0;
}
