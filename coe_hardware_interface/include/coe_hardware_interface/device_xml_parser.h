#ifndef __XML_DEVICE_PARSER__
#define __XML_DEVICE_PARSER__

#include <ros/console.h>
#include <ros/ros.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>
namespace pt = boost::property_tree;
namespace hardware_interface
{

class DeviceParser
{
public:
  DeviceParser(const std::__cxx11::string& deviceclass_name);
  ~DeviceParser();
  
  bool parseXml(const std::__cxx11::string& xml_value);
  bool parseYaml(const XmlRpc::XmlRpcValue& config);
  void mapMemory(const std::__cxx11::string& mem_name, const bool& readonly, const bool& open_only);
  bool read(  std::map<std::string,double>& data );
  bool write( const std::map<std::string,double>& data );
  std::map<std::string,double> getEmptyMap();
  unsigned int size(){return m_type.size();};
  std::vector<std::string> getNodeUniqueIDs(){return m_name;};
protected:
  
  void computeMemSize();
  
  std::vector<std::string> m_type;
  std::vector<std::string> m_name;
  std::vector<double> m_scale;
  std::vector<double> m_offset;
  boost::shared_ptr<boost::interprocess::mapped_region> m_mapmem;
  boost::shared_ptr<boost::interprocess::named_mutex> m_mtx;
  boost::interprocess::shared_memory_object m_memobj;
  std::__cxx11::string m_mem_name;
  long int  m_int8_t_size;
  std::__cxx11::string m_deviceclass_name;
  
};
}











#endif