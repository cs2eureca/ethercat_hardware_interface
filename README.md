![Alt text](media/logo-eureca-3.png)

# ETHERCAT HARDWARE INTERFACE #

The package has been developed within the scope of the project [EURECA](www.cleansky-eureca.eu/), funnded from the [Clean Sky](www.cleansky.eu) Joint Undertaking under the [European Union’s Horizon 2020]](https://ec.europa.eu/programmes/horizon2020/)  research and innovation programme under grant agreement nº 738039

> _The EURECA project framework is dedicated to innovate the assembly of aircraft interiors using advanced human-robot collaborative solutions. A pool of devices/frameworks will be deployed for teaming up with human operators in a human-centred assistive environment. The main benefits are the substantial improvement of ergonomics in workloads, the increase in the usability level of assembly actions, the digitalization of procedures with logs and visuals for error prevention through dedicated devices. Solutions are peculiarly designed for addressing both the working conditions and the management of the cabin-cargo installation process, such as limited maneuvering space, limited weight allowed on the cabin floor, reducing lead time and recurring costs. With this aim, EURECA will bring together research advancements spanning across design, manufacturing and control, robotized hardware, and software for the specific use-cases in the cabin-cargo final assembly._



### CanOpen over Ethercat ###

The CanOpen over Ethercat, [CoE](https://www.can-cia.org/fileadmin/resources/documents/proceedings/2005_rostan.pdf) hereafter, is among the most vesatile and used can in the industrial field, thanks to its intrinsic powerful architecture.  The packes should be considered as an extension with planty of utilities of the FOSS project Simple Open EtherCAT Master [SOEM](https://github.com/ros-industrial/ethercat-soem).

Tha package has been designed in order to control the new empowering collaborative robot deployed in EURECA

![Alt text](media/immagine-progetto-small.jpg)


The package is structured in many ROS packages, and specifically:

Package Name | Description
---:|:---
	 [coe_soem_utilites](https://bitbucket.org/iras-ind/coe_soem_utilities/src/master/) | a set of functionalities that ease the development of SOEM based applciations
	 [coe_core](https://bitbucket.org/iras-ind/coe_core/src/master/) | a set of libraries that implements the most important CanOpen structures, and it provides some utilities to deploy DS402 (motor drivers) applications 
	 [coe_driver](https://bitbucket.org/iras-ind/coe_driver/src/master/) |  a ROS based node that deploys the SOEM  exploting a shared-mameory mechanism
	 [coe_hw_plugins](https://bitbucket.org/iras-ind/coe_hw_plugins/src/master/) | the plugins (based on pluginlib) that allows an easy deployment of client applications 
	 [ethercat_hardware_interface](https://bitbucket.org/iras-ind/coe_hw_plugins/src/master/) | an interfoace toward ROS_CONTROL architecture. 

##  ETHERCAT HARDWARE INTERFACE Design ##

The proposed packages deals with the management of complex control architectures. In fact, Ros\_control framework works well in simple scenarios, when there is a single controller plugin and the hardware work at the same frequency. However, advanced robotic systems required different control configurations depending on the actual system behavior. Each control configuration could be a complex control architecture, such as nested controller loops, and it could acts on multiple robots, for example multiarm systems or robotic arm on a linear guide, as shown in Figure below. 


![Alt text](media/fig1.jpg)

ROS control framework allows managing the controllers lifecycle in a single hardware interface (see http://wiki.ros.org/ros\_control/Ideas).

The package goal is to keep ROS\_control framework to manage the single hardware\_interface and its controllers, while create a toolbox to simplify the integration and the lifecycle of multiple hardware\_interfaces.

This limitation implies an increment of implementation efforts and a
lack of modularity. In fact, thinking at robotic arm on a linear guide
example, the user has to implement a new hardware interface, or to use the multi\_hardware\_interface\_manager, and to implement a custom controller, which implements the nested control algorithms.

A simplistic solution is represented by using hardware interfaces only
for the last layer and use nodes for the other ones, losing the flexibility provided by ROS control. A more flexible approach is to use
nodes that implement intermediate hardware interfaces as shown in Figure below.  

![Alt text](media/fig1.jpg)


The intermediate hardware interface will exchange messages with
the lower-level controllers.

A limitation of this approach is possible messages loses when the
control frequency has the same order of magnitude of 1 kHz. For this
reason, nodelets mechanism has been preferred, allowing the using of intraprocess shared memory instead of the network card.

The goal of the proposed package is to simplify the lifecycle managers of multiple hardware interfaces and controllers, possibly reusing existence packages.

## Connections ##

A configuration\_manager node provides simple services to manage the configuration lifecycle. Configurations are defined a set of controllers running on a set of hardware interface. The manager provides the following service:

1\) Start a configuration (automatically turn off the actual one)

	*rosservice call /configuration_manager/start_configuration "start_configuration: 'configuration1' strictness: 1"*

where the strictness argument has the same meaning of ROS control messages.

The server will upload the required nodelets, and therefore hardware interface, while stopping the non necessary ones. After that, required controllers are started.

It is important to stress that if two configurations share hardware interfaces or controllers, these elements will not restarted avoiding deadtime. A ROS bond, already provided by nodelet packages, mechanism is used to manage the error handling. In this way, when a hardware interface fails, the configuration manager will stop all the other hardware interfaces.
	

2\) Stop actual configuration:

	*rosservice call /configuration_manager/stop_configuration
	"strictness: 1"*

This service stops the actual configurations without starting a new one.

3\) List configurations:

	*rosservice call /configuration_manager/list_configurations "{}"*

## Configuration ##


This service provides the list of the available configurations and which
one is currently active.

Configurations are read from the ROS parameter (the default name is
control\_*configurations*) which has the following structure:

```yaml

control_configurations: 
  - name: "configuration1" 
    components:  
      - description: "a brief description of the component" 
        hardware_interface: "hardware_interface_1" 			# the name of the hardware interface 
        controller        : "controller_1" 
      - description: "a brief description of the component" 
        hardware_interface: "hardware_interface_1" 
        controller        : "controller_2"  
      - description: "a brief description of the component" 
        hardware_interface: "hardware_interface_2" 
        controller        : "controller_3" 					# it is strongly recommended (but not mandatory) to use 
        													# different names also for controllers loaded in different hardware interfaces 
  - name: "configuration2" 
    components:  
      - description: "a brief description of the component" 
        hardware_interface: "motor_velocity_hi" 
        controller        : "linkpos_to_vel_jnt1_1" 
      - description: "a brief description of the component" 	# different configurations can use 
      															# the same hardware interfaces or the same controllers 

        hardware_interface: "hardware_interface_1" 
        controller        : "controller_2"
        
```
